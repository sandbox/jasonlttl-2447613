<?php

/**
 * Example controller class for the health_restws_monitor resource.
 */
class RoleRestwsResourceController implements RestWSQueryResourceControllerInterface {

  /**
   * @see hook_entity_property_info()
   * @see RestWSResourceControllerInterface::propertyInfo()
   */
  public function propertyInfo() {
    return array(
      'rid' => array(
        'type' => 'integer',
        'label' => t('Id'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'name' => array(
        'type' => 'text',
        'label' => t('Name'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'weight' => array(
        'type' => 'integer',
        'label' => t('Weight'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'users' => array(
        'type' => 'list<integer>',
        'label' => t('Description'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
    );
  }

  private function init() {
    if (!is_array($this->roles)) {
      $query = db_select('role', 'r');
      $query->leftJoin('users_roles', 'ur', 'r.rid = ur.rid');
      $query->fields('r', array('rid', 'name', 'weight'));
      $query->addExpression("GROUP_CONCAT(ur.uid SEPARATOR ',')", 'users');
      $query->groupBy('r.rid');
      $query->orderBy('weight');
      $query->orderBy('name');
      $result = $query->execute();

      $this->roles = array();
      foreach ($result as $role) {
        $role->users = explode(',', $role->users);
        $this->roles[$role->rid] = $role;
      }
    }
  }

  protected function get($id) {
    $this->init();
    $item = $this->roles[$id];
    return $item;
  }

  /**
   * @see RestWSResourceControllerInterface::wrapper()
   */
  public function wrapper($id) {
    $item = $this->get($id);
    $info = $this->propertyInfo();
    return entity_metadata_wrapper('role_restws_role', $item, array('property info' => $info));
  }

  /**
   * @see RestWSResourceControllerInterface::create()
   */
  public function create(array $values) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::read()
   */
  public function read($id) {
    $item = $this->get($id);
    return $item;
  }

  /**
   * @see RestWSResourceControllerInterface::update()
   */
  public function update($id, array $values) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::delete()
   */
  public function delete($id) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::access()
   */
  public function access($op, $id) {
    return TRUE;
  }

  /**
   * @see RestWSResourceControllerInterface::resource()
   */
  public function resource() {
    return 'role_restws_role';
  }

  public function query($filters = array(), $meta_controls = array()) {
    $this->init();
    $roles = array();
    foreach ($this->roles as $role) {
      $match = TRUE;
      foreach ($filters as $key => $val) {
        $match = $match && ($role->$key == $val);
      }
      if ($match) {
        $roles[] = $role->rid;
      }
    }
    return $roles;
  }

  public function count($filters = array()) {

  }

  public function limit($client_limit = NULL) {

  }
}